export interface Film {
  id_film: Number;
  titre: String;
  realisateur: String;
  score: Number;
  nbAwardsWin: Number;
  nbNominations: Number;
}