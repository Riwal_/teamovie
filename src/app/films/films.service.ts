import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";


@Injectable()
export class FilmsService {
  constructor(private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  //TO SET UP IN ENV VARIABLES

  //API d'appel
  private url = 'http://localhost:8080';


  getFilms() {
    return this
      .http
      .get(`${this.url}/movies`);
  }



  createFilm(data) {
    this.http.post(`${this.url}/movie`, data)
      .subscribe(
        res => {
          console.log(res);
          this.toastr.success('Votre film a été créé avec succès.', 'Success');
          this.router.navigateByUrl('/films');
        },
        err => {
          console.log('Error occured:', err);
          this.toastr.error(err.message, 'Error occured');
        }
      );
  }
}