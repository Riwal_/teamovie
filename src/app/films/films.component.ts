import { Component, OnInit } from '@angular/core';
import { FilmsService } from './films.service';
import { Film } from './films.interface';
import { Router } from "@angular/router";


@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit {

  films: Film[];

  constructor(private filmsService: FilmsService, private router: Router) {
  }

  ngOnInit() {
    this.filmsService.getFilms().subscribe((data => {

      var obj = this.parseJson(data.toString());

      for (var i = 0; i < Object.keys(obj).length; i++) {
        console.log("Object", obj[i]['_source']);
        const film: Film = {
          id_film: 1,
          titre: obj[i]['_source']['original_title'],
          realisateur: obj[i]['_source']['realisateur'],
          score: ['_source']['nbAwardsWin'] + 0.5 * ['_source']['nbNominations'],
          nbAwardsWin: ['_source']['nbAwardsWin'],
          nbNominations: ['_source']['nbNominations'],
        }
        this.films.push(film);
      }
      return this.films;
    }));
  }

  goToAddFilm() {
    this.router.navigateByUrl('/films/ajout-film');
  }

  parseJson(data) {
    data = data.replace('\n', '', 'g');

    var
      start = data.indexOf('{'),
      open = 0,
      i = start,
      len = data.length,
      result = [];

    for (; i < len; i++) {
      if (data[i] == '{') {
        open++;
      } else if (data[i] == '}') {
        open--;
        if (open === 0) {
          result.push(JSON.parse(data.substring(start, i + 1)));
          start = i + 1;
        }
      }
    }

    return result;
  }
}