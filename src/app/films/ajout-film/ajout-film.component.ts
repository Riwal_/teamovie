import { Component, OnInit } from '@angular/core';
import {FilmsService} from '../films.service';
import {Film} from "../films.interface";

@Component({
  selector: 'app-ajout-film',
  templateUrl: './ajout-film.component.html',
  styleUrls: ['./ajout-film.component.scss']
})
export class AjoutFilmComponent implements OnInit {

  /*
  films: Film = {
    id_film: null,
    titre: '',
    realisateur: null,
    score: null
  };
*/
  constructor(private filmService: FilmsService) { }

  ngOnInit() {
  }

  createFilm(data: Film) {
    this.filmService.createFilm(data);
  }

}