
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilmsComponent } from './films/films.component';
import { FilmsService } from "./films/films.service";
import { MaterialDashboardComponent } from './material-dashboard/material-dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import { ToastrModule } from "ngx-toastr";
import { AjoutFilmComponent } from './films/ajout-film/ajout-film.component';
import { ConfirmationPopoverModule } from "angular-confirmation-popover";
import { FormsModule } from "@angular/forms";

import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatGridListModule,
  MatCardModule,
  MatMenuModule
} from '@angular/material';

const appRoutes: Routes = [
  { path: 'films', component: FilmsComponent },
  { path: 'films/ajout-film', component: AjoutFilmComponent },

];
@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    MaterialDashboardComponent,
    AjoutFilmComponent,
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    ToastrModule.forRoot(),
    FormsModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    })
  ],

  providers: [
    FilmsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }