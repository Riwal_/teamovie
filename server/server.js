const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cors = require('cors');

const app = express();

const corsOptions = {
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200
}

const URL = "https://07e2f357ed5447199fd36fab863a3b0b.eu-west-1.aws.found.io:9243";
const USER_NAME = "elastic";
const USER_PASSWORD = "LzmIUFdRNjjNVbU41Ha8pczA";

app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.send('<h1>Accueil</h1>');
})

app.get('/movies', cors(corsOptions), function(req, res) {
  res.setHeader('Content-Type', 'text/json');
  getMovies().then(data => {
    res.json(data);
    console.log(data);
    res.send(data);
  });
})

app.post('/movie', function (req, res) {
    console.log(req.body);
    insertMovie(req.body);
    res.send(req.body);
})

function getMovies() {

    return axios.get(URL + '/test/12345/_search', {
        auth: {
            username: USER_NAME,
            password: USER_PASSWORD
        }
      })
      .then((response) => {
        let listMoviesJson = '';
        response.data.hits.hits.forEach(element => {
          listMoviesJson += JSON.stringify(element);
        });
        console.log(listMoviesJson);
        return listMoviesJson;
        return response.data.hits.hits;
      }, (error) => {
        console.log(error);
      });
}

function insertMovie(data) {
    axios({
        method: 'post',
        url: URL + '/test/12345',
        auth: {
            username: USER_NAME,
            password: USER_PASSWORD
        },
        data: data
    })
    .then((response) => {
        console.log(response.data);
      }, (error) => {
        console.log(error);
      });

}

app.listen(8080);