# ProjetMovie


**Get movie awards by movie genre.**


**Goal** : For each movie genre (comedy, horror, drama ...) we want to know how many awards it gets and which movie genre is the most succesfull each year.

API used : http://www.omdbapi.com/

We use omdb, the imdb api, to retrieve each movie with it's awards.

With our API we expose few functions : 

/genres         -> list of all movie genres with number of awards won and nominationseach year
/genres/{year}  -> list of all movie genres with number of awards won and nominations for a specific year
/movies         -> list of all movies with number of awards won and number of nominations
/movies/{year}  -> list of all movies with number of awards won and number of nominations for a specific year



Création application angular pour le front avec node js pour le back et elastic search pour la "recherche de données"


https://medium.com/code-divoire/creation-dune-application-web-avec-node-js-et-angular-v6-partie-2-frontend-820c998f6996


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
